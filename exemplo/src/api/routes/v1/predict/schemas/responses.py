from pydantic import BaseModel

class PredictResponse(BaseModel):
    iris_classification: str