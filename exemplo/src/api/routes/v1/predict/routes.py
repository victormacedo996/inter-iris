from fastapi import APIRouter, status
from api.routes.v1.predict.schemas import responses
from api.routes.v1.predict.schemas import requests
from api.service.prediction import predict
import pandas as pd
import sys
import logging


logging.basicConfig(level="INFO", stream=sys.stdout)


router = APIRouter(
    prefix="/predict",
    tags=["predict"],
)

@router.post(path="/", status_code=status.HTTP_200_OK, response_model=responses.PredictResponse)
async def inference(request: requests.PredictRequest) -> dict:
    try:
        inference_df = pd.DataFrame(data={
                "sepal_length": [request.sepal_length],	
                "sepal_width": [request.sepal_width],	
                "petal_length": [request.petal_length],	
                "petal_width": [request.petal_width]
            }
        )

        prediction = await predict(inference_df)

        return {
            "iris_classification": prediction
        }
    except:
        return {
            "error": "error in inference"
        }
