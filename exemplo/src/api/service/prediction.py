import pathlib
import pickle as pkl
from sklearn.base import BaseEstimator
import pandas as pd


ml_model: None | BaseEstimator = None

try:
    current_path = pathlib.Path(__file__).resolve().parent
    with open(f"{current_path}/../artifacts/model.pkl", 'rb') as model:
        ml_model = pkl.load(model)
except Exception as e:

    raise ValueError(f"Could not load model because of {e}")

async def predict(input_data: pd.DataFrame) -> str:
    mapping_classification = {
        0: "setosa",
        1: "versicolor",
        2: "virginica",
    }
    prediction = ml_model.predict(input_data)
    return mapping_classification[prediction[0]]